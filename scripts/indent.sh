#!/bin/sh

set -eu

( cd src

  for f in $(ls | grep .ml); do
    ocp-indent $f > tmp && mv tmp $f
  done

)
